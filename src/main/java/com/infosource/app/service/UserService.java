package com.infosource.app.service;

import java.util.List;

import com.infosource.app.dto.UserRequestDto;
import com.infosource.app.model.User;

public interface UserService {
	
	public List<User> getAll();
	
	public User getById(Integer id);
	
	public User create(UserRequestDto userRequestDto);
	
	public User update(Integer id, UserRequestDto userRequestDto);
	
	public void delete(Integer id);

}
