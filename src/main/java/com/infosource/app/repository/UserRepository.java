package com.infosource.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infosource.app.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
